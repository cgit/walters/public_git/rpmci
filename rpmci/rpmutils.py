#!/usr/bin/python
# rpmutils.py:
#
# Licensed under the new-BSD license (http://www.opensource.org/licenses/bsd-license.php)
# Copyright (C) 2010 Red Hat, Inc.
# Written by Colin Walters <walters@verbum.org>

def get_rpm_name(filename):
    (name, ver, rest) = filename.rsplit('-', 2)
    return name
        
