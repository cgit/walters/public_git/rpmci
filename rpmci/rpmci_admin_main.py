#!/usr/bin/python

# rpmci_admin_main.py:
# Implementation of rpmci-admin
#
# Licensed under the new-BSD license (http://www.opensource.org/licenses/bsd-license.php)
# Copyright (C) 2010 Red Hat, Inc.
# Written by Colin Walters <walters@verbum.org>

import os
import sys
import time
import shutil
import optparse
import logging
from ConfigParser import SafeConfigParser
import subprocess
import datetime

import glib
import gobject
import gio

from . import artifact
from . import msgqueue

def rebuild_one(options, config, args):
    if len(args) != 1:
        print "Usage: rebuild-one MODULE"
        sys.exit(1)
    module = args[0]
    artifact_set = artifact.ArtifactSet.from_config(config)
    target = None
    for target_iter in artifact_set.get_build_targets():
        if target_iter.module == module:
            target = target_iter
            break
    if target is None:
        raise SystemExit("Couldn't find module %r in configuration" % (module, ))
    upstream_url = artifact.upstream_vcs_url_for_build_target(config, target)

    vcs_msgqueue_dir = config.get('VCS', 'msgqueue')
    vcs_msgqueue = msgqueue.MessageQueue(vcs_msgqueue_dir)
    msg = msgqueue.Message(None, {'type': 'update'}, {'url': upstream_url})
    vcs_msgqueue.append(msg)
    print "Wrote %s" % (msg.ident, )

def rebuild_all(options, config, module):
    build_msgqueue_dir = config.get('build', 'msgqueue')
    build_msgqueue = msgqueue.MessageQueue(build_msgqueue_dir)

    msg = msgqueue.Message(None, {}, {'rebuild': True})
    build_msgqueue.append(msg)
    
    print "Wrote %s" % (msg.ident, )

def main():
    if hasattr('glib', 'threads_init'):
        glib.threads_init()

    opts = optparse.OptionParser("usage: %prog [options] ACTION [args]")
    opts.add_option('-c', '--config', dest='config', help="Path to configuration file")
    opts.add_option('', '--debug', action='store_true', help="Print verbose debugging")

    (options, args) = opts.parse_args()

    if options.config is None:
        print "Must specify --config"
        sys.exit(1)

    config = SafeConfigParser({'home': os.environ['HOME']})
    config.read(options.config)
    level = logging.DEBUG if options.debug else logging.INFO
    logging.basicConfig(stream=sys.stderr, level=level)

    if len(args) < 1:
        print "Must specify a valid command:"
        print "  rebuild-one MODNAME"
        print "  rebuild-all"
        opts.print_usage()
        sys.exit(1)

    if args[0] == 'rebuild-one':
        rebuild_one(options, config, args[1:])
    elif args[0] == 'rebuild-all':
        rebuild_all(options, config, args[1:])
    else:
        print "Invalid command"

    sys.exit(0)
