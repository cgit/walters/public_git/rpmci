#!/usr/bin/python

# artifact.py:
# Set of RPMs built into a repository.
#
# Licensed under the new-BSD license (http://www.opensource.org/licenses/bsd-license.php)
# Copyright (C) 2010 Red Hat, Inc.
# Written by Colin Walters <walters@verbum.org>

import os
import sys
import urllib
import ConfigParser

from . import spec

FEDORA_ANONGIT_URL = 'git:git://pkgs.fedoraproject.org'

def get_default_architectures():
    # There's probably some RPM thingy to find this
    uname = os.uname()
    arch = uname[-1]
    if arch == 'x86_64':
        # On x86_64, we can build both
        return ('i686', 'x86_64')
    elif arch == 'i686':
        # 32 bit can only build itself
        return 'i686'
    raise SystemExit("Unhandled architecture %r from os.uname(), known=[x86_64, i686]" % (arch, ))

class BuildTarget(object):
    def __init__(self, module, os, architecture):
        self.module = module
        self.os = os
        self.architecture = architecture

    def __cmp__(self, other):
        i = cmp(self.module, other.module)
        if i != 0:
            return i
        i = cmp(self.os, other.os)
        if i != 0:
            return i
        i = cmp(self.architecture, other.architecture)
        if i != 0:
            return i
        return 0

class Artifact(object):
    def __init__(self, name, os, architectures, targets):
        self.name = name
        self.os = os
        self.architectures = architectures
        self.targets = targets 

class ArtifactSet(object):
    def __init__(self, artifacts):
        self.artifacts = artifacts

    def get_build_targets(self):
        """Returns unordered set of build targets."""
        
        targets = set()
        for artifact in self.artifacts:
            for target in artifact.targets:
                targets.add(target)
        return targets

    @classmethod
    def from_config(cls, config):
        section = 'releases'
        artifacts_str = config.get(section, 'artifacts')
        artifact_list = map(str.strip, artifacts_str.split(' '))
        artifacts = []
        
        for artifact_name in artifact_list:
            artifact_build_targets = []

            config_name = artifact_name.replace('-', '_')

            modules = config.get(section, 'artifact_%s_modules' % (config_name, )).split(' ')
            modules = map(str.strip, modules)
            os = config.get(section, 'artifact_%s_os' % (config_name, ))
            try: 
                architectures = config.get(section, 'artifact_%s_architectures' % (config_name, )).split(' ')
            except ConfigParser.NoOptionError, e:
                architectures = get_default_architectures()
            architectures = map(str.strip, architectures)
            
            for module in modules:
                for architecture in architectures:
                    target = BuildTarget(module, os, architecture)
                    artifact_build_targets.append(target)

            artifacts.append(Artifact(artifact_name,
                                      os,
                                      architectures,
                                      artifact_build_targets))
        return cls(artifacts)

def fedora_git_url_for_build_target(config, buildtarget):
    fedora_os_master = config.get('fedora', 'master')
    base = '%s/%s.git' % (FEDORA_ANONGIT_URL, buildtarget.module)
    if buildtarget.os == fedora_os_master:
        return base
    else:
        return '%s#%s/master' % (base, buildtarget.os)

def upstream_vcs_url_for_build_target(config, buildtarget):
    fedora_url = fedora_git_url_for_build_target(config, buildtarget)
    escaped_url = urllib.quote(fedora_url, '')
    mirror_dir = config.get('VCS', 'mirror_dir')
    vcsdir = os.path.join(mirror_dir, escaped_url)
    if not os.path.isdir(vcsdir):
        raise SystemExit("Not a directory: %r" % (vcsdir, ))
    specpath = None
    for filename in os.listdir(vcsdir):
        if filename.endswith('.spec'):
            specpath = os.path.join(vcsdir, filename)
            break
    assert specpath is not None
    spec_obj = spec.Spec(specpath)
    return spec_obj.get_vcs()

