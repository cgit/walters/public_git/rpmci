#!/usr/bin/python

# rpmci_update_config_main.py:
# Implementation of rpmci-update-config
#
# Licensed under the new-BSD license (http://www.opensource.org/licenses/bsd-license.php)
# Copyright (C) 2010 Red Hat, Inc.
# Written by Colin Walters <walters@verbum.org>

import os
import sys
import time
import shutil
import optparse
from ConfigParser import SafeConfigParser
import logging
import urllib
import urlparse
import subprocess

import glib
import gobject
import gio

from . import msgqueue
from . import artifact
from . import spec

def _write_vcs_urls(options, config, urls):
    mirror_dir = config.get('VCS', 'mirror_dir')
    f = open(os.path.join(mirror_dir, 'vcs.txt'), 'w')
    for url in urls:
        f.write(url)
        f.write('\n')
    f.close()

def _run_vcsmirror(options, config):
    exec_basedir = os.path.dirname(sys.argv[0])

    args = [os.path.join(exec_basedir, 'rpmci-vcs-mirror'),
            '--config', options.config,
            '--clone-then-exit']
    print "Running: %r" % (args, )
    subprocess.check_call(args, stdout=sys.stdout, stderr=sys.stderr)

def update_config(options, config):
    mirror_dir = config.get('VCS', 'mirror_dir')

    if not os.path.isdir(mirror_dir):
        os.makedirs(mirror_dir)
    
    artifact_set = artifact.ArtifactSet.from_config(config)

    fedora_git_urls = set()

    unique_buildtargets = artifact_set.get_build_targets()
   
    for target in unique_buildtargets:
        url = artifact.fedora_git_url_for_build_target(config, target)
        fedora_git_urls.add(url)
        
    _write_vcs_urls(options, config, fedora_git_urls)
    _run_vcsmirror(options, config)
                                        
    all_urls = set(fedora_git_urls)
    for target in unique_buildtargets:
        upstream_vcs_url = artifact.upstream_vcs_url_for_build_target(config, target)
        all_urls.add(upstream_vcs_url)
            
    _write_vcs_urls(options, config, all_urls)
    _run_vcsmirror(options, config)

def main():
    if hasattr('glib', 'threads_init'):
        glib.threads_init()

    opts = optparse.OptionParser("usage: %prog [options]")
    opts.add_option('-c', '--config', dest='config', help="Path to configuration file")
    opts.add_option('', '--debug', action='store_true', help="Print verbose debugging")

    (options, args) = opts.parse_args()

    if options.config is None:
        print "Must specify --config"
        sys.exit(1)

    config = SafeConfigParser({'home': os.environ['HOME']})
    config.read(options.config)
    level = logging.DEBUG if options.debug else logging.INFO
    logging.basicConfig(stream=sys.stderr, level=level)

    update_config(options, config)
    sys.exit(0)
