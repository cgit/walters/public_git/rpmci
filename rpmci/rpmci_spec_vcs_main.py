#!/usr/bin/python

# rpmci_spec_vcs_main.py:
# Implementation of rpmci-spec-vcs
#
# Licensed under the new-BSD license (http://www.opensource.org/licenses/bsd-license.php)
# Copyright (C) 2010 Red Hat, Inc.
# Written by Colin Walters <walters@verbum.org>

import os
import sys
import time
import shutil
import optparse
import logging
import datetime

import glib
import gobject
import gio

from . import lame_vcs_abstraction
from . import spec

def _get_spec(options):
    dirpath = os.getcwd()
    specpath = None
    for filename in os.listdir(dirpath):
        if filename.endswith('.spec'):
            specpath = os.path.join(dirpath, filename)
            break
    if specpath is None:
        raise SystemExit("Couldn't find .spec file in %r" % (dirpath, ))
    return specpath

def set_revision(options, args):
    if len(args) == 0:
        print "usage: set-revision COMMITID"
        options.print_usage()
        sys.exit(1)

    revision = args[0]
    vcsdir = options.vcsdir or os.getcwd()

    specpath = _get_spec(options)
    spec_obj = spec.Spec(specpath)
    vcs_url = spec_obj.get_vcs()

    vcs = lame_vcs_abstraction.Vcs.new_from_spec(vcs_url)
    vcs.set_directory(vcsdir)
    resolved_id = vcs.resolve_id(revision)
    name = os.path.basename(spec_obj.get_name())
    prefix = '%s-%s' % (name, resolved_id)
    target_file = os.path.join(os.getcwd(), '%s.tar.bz2' % (prefix, ))
    vcs.export_archive(resolved_id, prefix, target_file, sys.stderr) 

    now = datetime.datetime.now()
    alphatag = "%s%s%s" % (now.strftime("%Y%m%d"), vcs.vcstype, resolved_id)
    spec_obj.increment_release_snapshot(alphatag)
    spec_obj.set_source(prefix, os.path.basename(target_file))
    spec_obj.save()

def main():
    if hasattr('glib', 'threads_init'):
        glib.threads_init()

    opts = optparse.OptionParser("usage: %prog [options] ACTION [args]")
    opts.add_option('', '--debug', action='store_true', help="Print verbose debugging")
    opts.add_option('', '--vcsdir', action='store', dest="vcsdir", 
                    default=None, help="Path to repository (defaults to current dir)")

    (options, args) = opts.parse_args()

    if len(args) < 1:
        opts.print_usage()
        sys.exit(1)

    verb = args[0]

    if verb == 'set-revision':
        set_revision(options, args[1:])
    else:
        opts.print_usage()
        sys.exit(1)

    sys.exit(0)
